/**
 * Created by stavr on 10/3/2016.
 */

"use strict";


const UniversityController = require("../controllers/university.controller"),
    hasRole = require('../middlewares/hasRole').hasRole,
    requireAuth = require('../middlewares/requireAuth').requireAuth,
    express = require('fwsp-hydra-express').getExpress(),
    paginate = require('express-paginate'),

    router = express.Router();



router
    .post('/', requireAuth, hasRole('Admin'), UniversityController.createUniversity)
    .get('/', UniversityController.getAllUniversities)
    .get('/:id', requireAuth, UniversityController.getUniversityById) // Get University by id
    .delete('/:id', requireAuth, hasRole('Admin'), UniversityController.deleteUniversity)
    .patch('/:id', requireAuth, hasRole('Admin'), UniversityController.updateUniversity);

module.exports = router;