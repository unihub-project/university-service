/**
 * Created by bdros on 12-Mar-17.
 */
'use strict';

const i18n = require('i18n'),
    hydra = require('fwsp-hydra-express').getHydra();


exports.hasRole = (role) => {
    return (req, res, next) => {
        hydra.makeAPIRequest(hydra.createUMFMessage({
            to: 'authentication-service:[get]/api/user',
            from: 'university-service:/',
            body: {}
        })).then((response) => {
            if(response.user.role == role) {
                return next();
            } else {
                return res.status(403).json({
                    title: i18n.__('FORBIDDEN'),
                    error: {message: i18n.__('FORBIDDEN')}
                })
            }
        })
            .catch((err) => {
                console.log(err);
            });
    }

};