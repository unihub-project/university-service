/**
 * Created by bdrosatos on 25/2/2017.
 */
'use strict';

const hydra = require('fwsp-hydra-express').getHydra();

exports.requireAuth = (req, res, next) => {
    hydra.makeAPIRequest(hydra.createUMFMessage({
        to: 'authentication-service:[get]/api/users/is/authenticated',
        from: 'university-service:/',
        body: {}
    })).then((response) => {
        if(response.statusCode == 200) {
            req.user = response.user;
            next();
        } else {
            return res.status(401).json(response);
        }
    })
        .catch((err) => {
            console.log(err);
        });
};