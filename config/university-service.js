/**
 * Created by bdros on 12-Mar-17.
 */
module.exports = {
    "hydra": {
        "serviceName": "university-service",
        "serviceDescription": "",
        "servicePort": process.env.PORT || 5001,
        "redis": {
            "url": "127.0.0.1",
            "port": 6379,
            "db" : 15
        }
    }
};